module MarsExploration
  autoload :ControlCenter, "#{File.dirname(__FILE__)}/mars_exploration/control_center"
  autoload :UserInput,     "#{File.dirname(__FILE__)}/mars_exploration/user_input"
  autoload :Parser,        "#{File.dirname(__FILE__)}/mars_exploration/parser"
  autoload :Plateau,       "#{File.dirname(__FILE__)}/mars_exploration/plateau"
  autoload :Rover,         "#{File.dirname(__FILE__)}/mars_exploration/rover"

  def self.run
    ControlCenter.new.run
  end
end
