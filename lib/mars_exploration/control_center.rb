module MarsExploration
  class ControlCenter
    FIRST_ROVER_NAME = :first
    SECOND_ROVER_NAME = :second

    def run
      plateau_coordinates = UserInput::PlateauCoordinates.call(Parser::PlateauCoordinates.new)
      first_rover_position = UserInput::RoverPosition.call(Parser::RoverPosition.new, FIRST_ROVER_NAME)
      first_rover_instructions = UserInput::RoverInstructions.call(Parser::RoverInstructions.new, FIRST_ROVER_NAME)
      second_rover_position = UserInput::RoverPosition.call(Parser::RoverPosition.new, SECOND_ROVER_NAME)
      second_rover_instructions = UserInput::RoverInstructions.call(Parser::RoverInstructions.new, SECOND_ROVER_NAME)

      plateau = Plateau.new(plateau_coordinates.x_max, plateau_coordinates.y_max)
      first_rover = create_rover(FIRST_ROVER_NAME, first_rover_position, plateau)
      second_rover = create_rover(SECOND_ROVER_NAME, second_rover_position, plateau)

      first_rover_instructions.list.each { |instruction| first_rover.execute(instruction) }
      second_rover_instructions.list.each { |instruction| second_rover.execute(instruction) }

      puts first_rover
      puts second_rover
    end

    private

    def create_rover(name, position, plateau)
      Rover.new(name: name, x: position.x, y: position.y, direction: position.direction, plateau: plateau)
    end
  end
end
