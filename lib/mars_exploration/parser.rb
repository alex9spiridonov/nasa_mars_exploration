module MarsExploration
  class Parser
    autoload :PlateauCoordinates, "#{File.dirname(__FILE__)}/parser/plateau_coordinates"
    autoload :RoverInstructions,  "#{File.dirname(__FILE__)}/parser/rover_instructions"
    autoload :RoverPosition,      "#{File.dirname(__FILE__)}/parser/rover_position"

    def parse(string)
      raise NotImplementedError, 'Should be implemented in the child class!'
    end
  end
end
