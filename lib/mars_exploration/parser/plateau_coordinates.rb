module MarsExploration
  class Parser::PlateauCoordinates < Parser
    attr_reader :x_max, :y_max

    def parse(string)
      result = string.match(/(\d+)\s(\d+)/)
      return unless result

      @x_max = result[1].to_i
      @y_max = result[2].to_i
    end
  end
end
