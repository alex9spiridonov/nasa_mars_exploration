module MarsExploration
  class Parser::RoverInstructions < Parser
    attr_reader :list

    def parse(string)
      result = string.scan(/L|R|M/)
      return if result.empty?

      @list = result.map(&:to_sym)
    end
  end
end
