module MarsExploration
  class Parser::RoverPosition < Parser
    attr_reader :x, :y, :direction

    def parse(string)
      result = string.match(/(\d+)\s(\d+)\s(N|E|S|W)/)
      return unless result

      @x = result[1].to_i
      @y = result[2].to_i
      @direction = result[3].to_sym
    end
  end
end
