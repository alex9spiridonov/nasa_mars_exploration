module MarsExploration
  class Plateau
    attr_reader :x_min, :y_min, :x_max, :y_max

    def initialize(x_max, y_max, x_min = 0, y_min = 0)
      @x_min = x_min
      @y_min = y_min
      @x_max = x_max
      @y_max = y_max
    end

    def include_point?(x, y)
      x_min <= x && x <= x_max && y_min <= y && y <= y_max
    end
  end
end
