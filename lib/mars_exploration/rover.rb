module MarsExploration
  class Rover
    attr_reader :name, :x, :y, :direction, :plateau

    def initialize(name:, x:, y:, direction:, plateau:)
      @plateau = plateau
      @name = name
      @direction = direction

      unless available_point?(x, y)
        raise "#{name.capitalize} rover can't land at point (#{x}, #{y}). The point is outside the plateau."
      end

      @x = x
      @y = y
    end

    def execute(command)
      case command
      when :R then rotate_right
      when :L then rotate_left
      when :M then move
      else raise "Invalid command for #{name} rover!"
      end
    end

    def to_s
      "#{name.capitalize} rover coordinates: #{x} #{y} #{direction}"
    end

    private

    def rotate_right
      @direction =
        case direction
        when :N then :E
        when :E then :S
        when :S then :W
        when :W then :N
        end
    end

    def rotate_left
      @direction =
        case direction
        when :N then :W
        when :W then :S
        when :S then :E
        when :E then :N
        end
    end

    def move
      planned_x = x
      planned_y = y

      case direction
      when :N then planned_y += 1
      when :E then planned_x += 1
      when :S then planned_y -= 1
      when :W then planned_x -= 1
      end

      unless available_point?(planned_x, planned_y)
        raise "#{name.capitalize} rover can't move to point (#{planned_x}, #{planned_y})." \
              ' The point is outside the plateau.'
      end

      @x = planned_x
      @y = planned_y
    end

    def available_point?(x, y)
      plateau.include_point?(x, y)
    end
  end
end
