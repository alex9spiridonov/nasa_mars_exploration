module MarsExploration
  class UserInput
    autoload :PlateauCoordinates, "#{File.dirname(__FILE__)}/user_input/plateau_coordinates"
    autoload :RoverInstructions,  "#{File.dirname(__FILE__)}/user_input/rover_instructions"
    autoload :RoverPosition,      "#{File.dirname(__FILE__)}/user_input/rover_position"

    attr_reader :parser, :rover_name

    def self.call(parser, rover_name = nil)
      new(parser, rover_name).call
    end

    def initialize(parser, rover_name)
      @parser = parser
      @rover_name = rover_name
    end

    def call
      loop do
        puts info_request_message
        break parser if parser.parse(STDIN.gets.chomp)

        puts failed_info_message
      end
    end

    private

    def info_request_message
      raise NotImplementedError, 'Should be implemented in the child class!'
    end

    def failed_info_message
      'Invalid input!'
    end
  end
end
