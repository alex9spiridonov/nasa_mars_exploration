module MarsExploration
  class UserInput::PlateauCoordinates < UserInput
    private

    def info_request_message
      'Enter upper-right coordinates of the plateau:'
    end
  end
end
