module MarsExploration
  class UserInput::RoverInstructions < UserInput
    private

    def info_request_message
      "Enter the series of instructions for the #{rover_name} rover:"
    end
  end
end
