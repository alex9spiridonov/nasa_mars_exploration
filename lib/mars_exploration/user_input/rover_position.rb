module MarsExploration
  class UserInput::RoverPosition < UserInput
    private

    def info_request_message
      "Enter the position of the #{rover_name} rover:"
    end
  end
end
