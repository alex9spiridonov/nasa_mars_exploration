require './lib/mars_exploration/parser'
require './lib/mars_exploration/parser/plateau_coordinates'

describe MarsExploration::Parser::PlateauCoordinates do
  describe '#parse' do
    subject(:parser) { described_class.new }

    context 'when the input string is valid' do
      before { parser.parse('5 5') }

      it 'should return coordinates' do
        expect(parser.x_max).to eq(5)
        expect(parser.y_max).to eq(5)
      end
    end

    context 'when the input string is invalid' do
      before { parser.parse('5') }

      it 'should return nil' do
        expect(parser.x_max).to be_nil
        expect(parser.y_max).to be_nil
      end
    end
  end
end
