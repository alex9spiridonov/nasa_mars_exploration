require './lib/mars_exploration/parser'
require './lib/mars_exploration/parser/rover_instructions'

describe MarsExploration::Parser::RoverInstructions do
  describe '#parse' do
    subject(:parser) { described_class.new }

    context 'when the input string is valid' do
      before { parser.parse('MLMRMMLR') }

      it 'should return a list of instructions' do
        expect(parser.list).to eq(%i[M L M R M M L R])
      end
    end

    context 'when the input string is invalid' do
      before { parser.parse('ASDFG') }

      it 'should return nil' do
        expect(parser.list).to be_nil
      end
    end
  end
end
