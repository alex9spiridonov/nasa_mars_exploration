require './lib/mars_exploration/parser'
require './lib/mars_exploration/parser/rover_position'

describe MarsExploration::Parser::RoverPosition do
  describe '#parse' do
    subject(:parser) { described_class.new }

    context 'when the input string is valid' do
      before { parser.parse('1 2 N') }

      it 'should return position' do
        expect(parser.x).to eq(1)
        expect(parser.y).to eq(2)
        expect(parser.direction).to eq(:N)
      end
    end

    context 'when the input string is invalid' do
      before { parser.parse('5 22') }

      it 'should return nil' do
        expect(parser.x).to be_nil
        expect(parser.y).to be_nil
        expect(parser.direction).to be_nil
      end
    end
  end
end
