require './lib/mars_exploration/plateau'

# TODO: add factory_bot
describe MarsExploration::Plateau do
  describe '#include_point?' do
    subject(:plateau) { described_class.new(5, 5) }

    context 'when the point is inside the plateau' do
      it 'should return true' do
        expect(plateau.include_point?(1, 2)).to be_truthy
      end
    end

    context 'when the point is outside the plateau' do
      it 'should return false' do
        expect(plateau.include_point?(5, 6)).to be_falsey
      end
    end
  end
end
