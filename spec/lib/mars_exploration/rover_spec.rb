require './lib/mars_exploration/plateau'
require './lib/mars_exploration/rover'

describe MarsExploration::Rover do
  describe '#execute' do
    subject(:rover) { described_class.new(name: 'first', x: 2, y: 2, direction: :N, plateau: plateau) }

    let(:plateau) { MarsExploration::Plateau.new(5, 5) }

    context 'when turns to the right' do
      it 'should change direction' do
        expect { rover.execute(:R) }.to change { rover.direction }.to(:E)
      end
    end

    context 'when turns to the left' do
      it 'should change direction' do
        expect { rover.execute(:L) }.to change { rover.direction }.to(:W)
      end
    end

    context 'when moving' do
      context 'when it is possible to move' do
        it 'should change coordinates' do
          expect { rover.execute(:M) }.to change { rover.y }.to(3)
        end
      end

      context 'when it is not possible to move' do
        subject(:rover) { described_class.new(name: 'first', x: 2, y: 5, direction: :N, plateau: plateau) }

        it 'should raise error' do
          expect { rover.execute(:M) }
            .to raise_error(RuntimeError, 'First rover can\'t move to point (2, 6). The point is outside the plateau.')
        end
      end
    end
  end
end
