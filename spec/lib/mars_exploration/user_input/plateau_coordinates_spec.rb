require './lib/mars_exploration/parser'
require './lib/mars_exploration/user_input'
require './lib/mars_exploration/user_input/plateau_coordinates'

describe MarsExploration::UserInput::PlateauCoordinates do
  describe '#call' do
    subject(:user_input) { described_class.new(parser, :first) }

    let(:parser) { MarsExploration::Parser::PlateauCoordinates.new }

    context 'when the input string is invalid' do
      before do
        allow(STDIN).to receive(:gets).and_return('5', '5 5')
      end

      it 'retry input' do
        expect { user_input.call }.to output(
          <<~OUTPUT
            Enter upper-right coordinates of the plateau:
            Invalid input!
            Enter upper-right coordinates of the plateau:
          OUTPUT
        ).to_stdout
      end
    end

    context 'when the input string is valid' do
      before do
        allow(STDIN).to receive(:gets).and_return('5 5')
      end

      it 'do not retry input' do
        expect { user_input.call }.to output("Enter upper-right coordinates of the plateau:\n").to_stdout
      end

      it 'should return parser' do
        expect(user_input.call).to eq(parser)
      end
    end
  end
end
