require './lib/mars_exploration'

describe MarsExploration do
  describe '.run' do
    before do
      allow(STDIN).to receive(:gets).and_return('5 5', '1 2 N', 'LMLMLMLMM', '3 3 E', 'MMRMMRMRRM')
    end

    it do
      expect { described_class.run }.to output(
        <<~OUTPUT
          Enter upper-right coordinates of the plateau:
          Enter the position of the first rover:
          Enter the series of instructions for the first rover:
          Enter the position of the second rover:
          Enter the series of instructions for the second rover:
          First rover coordinates: 1 3 N
          Second rover coordinates: 5 1 E
        OUTPUT
      ).to_stdout
    end
  end
end
